import 'package:demoproject/sign_up.dart';
import 'package:flutter/material.dart';

class SignInScreen extends StatefulWidget {
  const SignInScreen({super.key});

  @override
  State<SignInScreen> createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: SizedBox(
            child: Image.asset(
              "assets/images/back3.png",
            ),
          ),
        ),
        backgroundColor: const Color.fromARGB(255, 229, 229, 229),
        leadingWidth: 62,
      ),
      body: SingleChildScrollView(
        physics: const NeverScrollableScrollPhysics(),
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: const Color.fromARGB(255, 229, 229, 229),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 20,
              ),
              const Padding(
                padding: EdgeInsets.symmetric(horizontal: 23),
                child: Text(
                  "Welcome Back!",
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: Color.fromARGB(255, 17, 25, 100),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 29),
                child: TextFormField(
                  decoration: InputDecoration(
                    prefixIcon: Image.asset("assets/images/user1.png"),
                    hintText: "Username",
                    hintStyle: const TextStyle(
                        fontSize: 15, fontWeight: FontWeight.w400),
                  ),
                ),
              ),
              const SizedBox(
                height: 35,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 29),
                child: TextFormField(
                  decoration: InputDecoration(
                    prefixIcon: Image.asset("assets/images/LOCK.png"),
                    hintText: "Password",
                    hintStyle: const TextStyle(
                        fontSize: 15, fontWeight: FontWeight.w400),
                    suffixText: "Forgot?",
                    suffixStyle: const TextStyle(
                      color: Color.fromARGB(255, 9, 15, 71),
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 50,
              ),
              Center(
                child: Container(
                  width: 320,
                  height: 50,
                  decoration: BoxDecoration(
                    color: const Color.fromARGB(255, 65, 87, 255),
                    borderRadius: BorderRadius.circular(50),
                    border: Border.all(
                        color: const Color.fromARGB(255, 65, 87, 255),
                        width: 2),
                  ),
                  child: const Center(
                    child: Text(
                      "LOGIN",
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1,
                        color: Color.fromARGB(255, 241, 237, 237),
                      ),
                    ),
                  ),
                ),
              ),
              const SizedBox(
                height: 132,
              ),
              Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      "< Don't have an account? ",
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Color.fromARGB(255, 9, 15, 71),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const SignUpPage()));
                      },
                      child: const Text(
                        "Sign Up",
                        style: TextStyle(
                          fontSize: 14,
                          fontWeight: FontWeight.w400,
                          color: Color.fromARGB(255, 9, 15, 71),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
