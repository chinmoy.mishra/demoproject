import 'package:demoproject/welcome.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  PageController controller = PageController();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    controller =
        PageController(initialPage: 0, keepPage: true, viewportFraction: 1.0);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 229, 229, 229),
      ),
      body: SizedBox(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: PageView(
          controller: controller,
          allowImplicitScrolling: true,
          children: [
            Container(
              color: const Color.fromARGB(255, 229, 229, 229),
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 380,
                    width: 270,
                    child: Image.asset(
                      "assets/images/Union.png",
                      scale: 1.0,
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 80),
                    child: Text(
                      textAlign: TextAlign.center,
                      "View and buy Medicine online",
                      style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                        color: Color.fromARGB(255, 17, 25, 100),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 60),
                    child: Text(
                        style: TextStyle(
                          fontSize: 16,
                          color: Color.fromARGB(255, 151, 151, 153),
                        ),
                        textAlign: TextAlign.center,
                        "Etiam mollis metus non purus faucibus sollicitudin. Pellentesque sagittis mi. Integer.,"),
                  ),
                ],
              ),
            ),
            Container(
              color: const Color.fromARGB(255, 229, 229, 229),
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 380,
                    width: 270,
                    child: Image.asset(
                      "assets/images/Group.png",
                      scale: 1.0,
                    ),
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 80),
                    child: Text(
                      textAlign: TextAlign.center,
                      "Online medical & Healthcare",
                      style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                        color: Color.fromARGB(255, 17, 25, 100),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 60),
                    child: Text(
                        style: TextStyle(
                          fontSize: 16,
                          color: Color.fromARGB(255, 151, 151, 153),
                        ),
                        textAlign: TextAlign.center,
                        "Etiam mollis metus non purus faucibus sollicitudin. Pellentesque sagittis mi. Integer."),
                  ),
                ],
              ),
            ),
            Container(
              color: const Color.fromARGB(255, 229, 229, 229),
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 380,
                    width: 270,
                    child: Image.asset(
                      "assets/images/Group1.png",
                      scale: 1.0,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 80),
                    child: Text(
                      textAlign: TextAlign.center,
                      "Get Delivery on time",
                      style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.bold,
                        color: Color.fromARGB(255, 17, 25, 100),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 60),
                    child: Text(
                        style: TextStyle(
                          fontSize: 16,
                          color: Color.fromARGB(255, 151, 151, 153),
                        ),
                        textAlign: TextAlign.center,
                        "Etiam mollis metus non purus faucibus sollicitudin. Pellentesque sagittis mi. Integer."),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      bottomSheet: Container(
        color: Color.fromARGB(255, 229, 229, 229),
        padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 30),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TextButton(
                style: TextButton.styleFrom(
                  foregroundColor:
                      const Color.fromARGB(146, 18, 29, 122), // Text Color
                ),
                onPressed: () {
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                          builder: (context) => const WelcomePage()),
                      (Route<dynamic> route) => false);
                  // Navigator.pushReplacement(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (context) => const WelcomePage()));
                },
                child: const Text(
                  "Skip",
                  style: TextStyle(
                    fontSize: 14,
                  ),
                  // selectionColor: Colors.grey,
                )),
            SmoothPageIndicator(
              controller: controller,
              count: 3,
              effect: const WormEffect(dotHeight: 10.0, dotWidth: 10.0),
            ),
            TextButton(
              style: TextButton.styleFrom(
                foregroundColor:
                    const Color.fromARGB(255, 65, 87, 255), // Text Color
              ),
              onPressed: () {
                controller.page == 2
                    ? Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(
                            builder: (context) => const WelcomePage()),
                        (Route<dynamic> route) => false)
                    // controller.nextPage(
                    //     duration: const Duration(milliseconds: 500),
                    //     curve: Curves.linear)
                    : controller.nextPage(
                        duration: const Duration(milliseconds: 500),
                        curve: Curves.linear);
              },
              child: const Text("Next"),
            ),
          ],
        ),
      ),
    );
  }
}
