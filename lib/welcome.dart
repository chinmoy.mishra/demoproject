import 'dart:ui';

import 'package:demoproject/sign_in.dart';
import 'package:flutter/material.dart';

class WelcomePage extends StatefulWidget {
  const WelcomePage({super.key});

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(255, 229, 229, 229),
      ),
      body: Container(
        color: const Color.fromARGB(255, 229, 229, 229),
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 340,
              width: 270,
              child: Image.asset(
                "assets/images/Mask.png",
                scale: 1.0,
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 80),
              child: Text(
                textAlign: TextAlign.center,
                "Welcome to Medtech",
                style: TextStyle(
                  fontSize: 22,
                  fontWeight: FontWeight.bold,
                  color: Color.fromARGB(255, 17, 25, 100),
                ),
              ),
            ),
            const SizedBox(
              height: 18,
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 60),
              child: Text(
                  style: TextStyle(
                    fontSize: 16,
                    color: Color.fromARGB(255, 151, 151, 153),
                  ),
                  textAlign: TextAlign.center,
                  "Do you want some help with your health to get better life?"),
            ),
            const SizedBox(
              height: 25,
            ),
            Container(
              width: 300,
              height: 50,
              decoration: BoxDecoration(
                color: const Color.fromARGB(255, 65, 87, 255),
                borderRadius: BorderRadius.circular(50),
                border: Border.all(
                    color: const Color.fromARGB(255, 65, 87, 255), width: 2),
              ),
              child: const Center(
                child: Text(
                  "SIGN UP WITH EMAIL",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    letterSpacing: 1,
                    color: Color.fromARGB(255, 241, 237, 237),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: BackdropFilter(
                filter: ImageFilter.blur(),
                // filter: ImageFilter.blur(
                //   sigmaX: widget.isTransParent ? 0.0 : 15.0,
                //   sigmaY: widget.isTransParent ? 0.0 : 15.0,
                // ),
                child: Container(
                  width: 300,
                  height: 50,
                  // width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    border: Border.all(
                      color: Color.fromARGB(60, 163, 161, 161),
                    ),
                    color: Colors.transparent,
                  ),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 12),
                      child: Row(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 20,
                            width: 50,
                            child: Image.asset("assets/images/facebook.png"),
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          const Text(
                            "CONTINUE WITH FACEBOOK",
                            style: TextStyle(
                                fontSize: 13,
                                fontWeight: FontWeight.bold,
                                letterSpacing: 1,
                                color: const Color.fromARGB(146, 18, 29, 122)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: BackdropFilter(
                filter: ImageFilter.blur(),
                // filter: ImageFilter.blur(
                //   sigmaX: widget.isTransParent ? 0.0 : 15.0,
                //   sigmaY: widget.isTransParent ? 0.0 : 15.0,
                // ),
                child: Container(
                  width: 300,
                  height: 50,
                  // width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    border: Border.all(
                      color: Color.fromARGB(60, 163, 161, 161),
                    ),
                    color: Colors.transparent,
                  ),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: Row(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            height: 20,
                            width: 50,
                            child: Image.asset("assets/images/google.png"),
                          ),
                          const SizedBox(
                            width: 2,
                          ),
                          const Text(
                            "CONTINUE WITH GOOGLE",
                            style: TextStyle(
                                fontSize: 13,
                                fontWeight: FontWeight.bold,
                                letterSpacing: 1,
                                color: const Color.fromARGB(146, 18, 29, 122)),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            TextButton(
              style: TextButton.styleFrom(
                foregroundColor:
                    const Color.fromARGB(255, 65, 87, 255), // Text Color
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const SignInScreen()));
              },
              child: const Text(
                "LOGIN",
                style: TextStyle(
                  fontSize: 14,
                  fontWeight: FontWeight.w400,
                  color: const Color.fromARGB(146, 18, 29, 122),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
